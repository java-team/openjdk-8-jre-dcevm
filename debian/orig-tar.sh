#!/bin/sh

PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
TAR=../${PACKAGE}_${VERSION}.orig.tar.gz
TAG=jdk8u181-b13

rm -f $3

wget http://hg.openjdk.java.net/jdk8u/jdk8u/hotspot/archive/$TAG.tar.gz -O $TAR
